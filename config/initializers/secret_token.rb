# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyBlog::Application.config.secret_key_base = 'c9e943110e481ef8645d0c70d3cc8981fe293844479f0b3f5e49f889993ab23fcb19f28640300a408044e225d175457612c7b2385865aeb185951d6c854489df'
